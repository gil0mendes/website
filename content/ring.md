# { } Ring

This page is a webring with websites that I found interesting and/or worth saving for future.

- [Mike Polinowski's Notebook](https://mpolinowski.github.io/) - Notes about development and DevOps.
- [Tony Finn's Blog](https://tonyfinn.com/) - Useful articles about Nix
